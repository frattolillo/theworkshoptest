import template from './app.html';
import './styles/app.scss';

let appComponent = {
  template,
  restrict: 'E'
};

export default appComponent;

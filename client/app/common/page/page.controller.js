//const src = require('./avatar.png');

class PageController {
  constructor($state, $filter, PageService) {
  	"ngInject";

  	this.name = 'page';
    this.$state = $state;
    this.$filter = $filter;
    this.PageService = PageService;
    this.content =  {
    	title: "",
    	body: ""
    }
  }
  $onInit(){
		 var that = this;

	     if(this.$state.current.name === "app.poker") {
	     	this.PageService.getContents().then(function(data) {
	     		that.content = that.$filter('filter')(data, {cat: 'poker'})[0];
		 	});
	     } else if(this.$state.current.name === "app.casino") {
	     	this.PageService.getContents().then(function(data) {
	     		that.content = that.$filter('filter')(data, {cat: 'casino'})[0];
		 	});
	     } else if(this.$state.current.name === "app.sports") {
	     	this.PageService.getContents().then(function(data) {
	     		that.content = that.$filter('filter')(data, {cat: 'sports'})[0];
		 	});
	     } else if(this.$state.current.name === "app.live") {
	     	this.PageService.getContents().then(function(data) {
	     		that.content = that.$filter('filter')(data, {cat: 'live'})[0];
		 	});
	     }

	}
}

export default PageController;

import angular              from 'angular';
import navigationComponent  from './navigation.component';
import toggleMenuDirective from './toggleMenu.directive';
//require('../../../bootstrap/dist/js/bootstrap');

let navigationModule = angular.module('navigation', [])

.component('navigation', navigationComponent)
.directive('toggleMenu', toggleMenuDirective);

export default navigationModule;

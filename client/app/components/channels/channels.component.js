import template from './channels.html';
import controller from './channels.controller';
//import './channels.scss';

let channelsComponent = {
  restrict: 'E',
  bindings: {},
  template,
  controller,
  controllerAs: 'vm'
};

export default channelsComponent;

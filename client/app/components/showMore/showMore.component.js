import template from './showMore.html';
import controller from './showMore.controller';
//import './showMore.scss';

let showMoreComponent = {
  restrict: 'E',
  bindings: {},
  template,
  controller,
  controllerAs: 'vm'
};

export default showMoreComponent;

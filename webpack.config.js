var path    = require('path');
var webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');
//var $, jQuery, jquery = require('jquery');

module.exports = {
  devtool: 'sourcemap',
  entry: {},
  resolve: {
    extensions: ['', '.js', '.css', '.scss']
  },
  module: {
    sassResources: './config/sass-resources.scss',
    loaders: [
                { test:/bootstrap-sass[\/\\]assets[\/\\]javascripts[\/\\]/, loader: 'imports-loader?jQuery=jquery' },
                { test: /\.js$/, exclude: [/app\/lib/, /node_modules/], loader: 'ng-annotate!babel' },

                { test: /\.html$/, loader: 'raw' },
                {
                test: /\.(png|jpg|ttf)$/,
                exclude: /node_modules/,
                loader: "url-loader?limit=1000000"
                },
                {
                   test   : /\.woff/,
                   loader : require.resolve("url-loader") + '?prefix=font/&limit=10000&mimetype=application/font-woff&name=assets/[hash].[ext]'
                },
                {
                   test   : /\.ttf/,
                   loader : require.resolve("file-loader") + '?prefix=font/&name=assets/[hash].[ext]'
                },
                {
                   test   : /\.eot/,
                   loader : require.resolve("file-loader") + '?prefix=font/&name=assets/[hash].[ext]'
                },
                {
                   test   : /\.svg/,
                   loader : require.resolve("file-loader") + '?prefix=font/&name=assets/[hash].[ext]'
                },

                { test: /\.scss$/, loader: 'style!css?sourceMap!sass?sourceMap' },
                { test: /\.css$/, loader: 'style!css' }
    ]
    
  },
  plugins: [
    // Injects bundles in your index.html instead of wiring all manually.
    // It also adds hash to all injected assets so we don't have problems
    // with cache purging during deployment.
    new HtmlWebpackPlugin({
      template: 'client/index.html',
      inject: 'body',
      hash: true
    }),
    new webpack.ProvidePlugin({
      $: 'jquery',
      jQuery: 'jquery',
      jquery: 'jquery'
    }),
    // Automatically move all modules defined outside of application directory to vendor bundle.
    // If you are using more complicated project structure, consider to specify common chunks manually.
    new webpack.optimize.CommonsChunkPlugin({
      name: 'vendor',
      minChunks: function (module, count) {
        return module.resource && module.resource.indexOf(path.resolve(__dirname, 'client')) === -1;
      }
    })
  ]
};

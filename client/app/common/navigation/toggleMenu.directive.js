let toggleMenuDirective = function() {
	
	function link(scope, elem, attrs) {

		elem.on('click', function() {
			$(elem).parent().parent().find('.collapse').slideToggle()
		});

	}
	return {
		link: link
	}
}

export default toggleMenuDirective;
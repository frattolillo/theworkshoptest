import angular from 'angular';
import uiRouter from 'angular-ui-router';
import channelsComponent from './channels.component';

const channelsModule = angular.module('channels', [
  uiRouter
])

.component('channels', channelsComponent);

export default channelsModule;

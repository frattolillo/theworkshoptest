import template from './page.html';
import controller from './page.controller';
//import './page.scss';

let pageComponent = {
  restrict: 'E',
  bindings: {},
  template,
  controller,
  controllerAs: 'vm'
};

export default pageComponent;
